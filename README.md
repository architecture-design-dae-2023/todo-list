# Proyecto Final Desarrollo de Aplicaciones Empresariales : ToDO-List


# Autores
- Juan Carlos Velasquez
- Diana Diaz
- Angel Parra
- Marlon Campaña

# Descripción

Como culminación del curso de Desarrollo de Aplicaciones Empresariales, dentro del marco de la maestría en Ingeniería de Software en la Universidad Politécnica Salesiana, hemos abordado el desafío del tema 3 en equipo. Este tema se centra en la elaboración de un proyecto ToDo List con las siguientes tecnologías:  
- FrontEnd: Vue.js 
- BackEnd: .Net Core / Quarkus

# Visión Arquitectónica

La solución para el proyecto ToDo List provista se implementará bajo los siguientes principios.
- Arquitectura unificada, con lineamientos comunes, particularmente arquitecturas basadas en servicios.
- Diferenciación componentes Capa FrontEnd y BackEnd.
- Soluciones basadas en componentes funcionales de fácil integración por tecnologías y estándares abiertos como REST.

Al diseñar la arquitectura, se puso un fuerte énfasis en dos características principales:
- La aplicación debe ser confiable
- La aplicación debe ser rápida.
- La aplicación debe ser cumplir con todo lo solicitado.

# Supuestos

Las siguientes suposiciones de arquitectura se basa en los objetivos del proyecto:
- El proyecto estará alojado en Microsoft Azure.
- El proyecto utilziará como tecnología FrontEnd Vue.js
- El proyecto utilziará como tecnología BackEnd .Net Core 


# Decisiones de arquitectura


La documentación de decisiones de arquitectura o Architecture Decision Records (ADR) es una colección de documentos que recogen individualmente cada una de las decisiones de arquitectura tomadas. 

|  A continuación las ADRs de esta solución |
|---|
| [Base de datos](adr/adr-noFuncional1.md)|
| [Selección de arquitectura de FrontEnd](adr/adr-noFuncional2.md) |
| [Selección de arquitectura de BackEnd](adr/adr-noFuncional3.md) |
| 


# Diagramas
Los diagramas estarán basados por el [modelo c4](https://c4model.com/)
Estos diagramas proporcionan diferentes niveles de abstracción, para un mejor entendimiento.

A continuación, se detallan los diagramas para esta solución: 

- Contexto.
- Contenedores.
- Componentes.
