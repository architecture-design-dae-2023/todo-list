# Base de Datos

## Estado

Propuesto

## Contexto

Para el proyecto se debe trabajar con SQLServer.

## Decisión

Se toma la decisión de utilizar SQLServer como base de datos. Por las siguientes razones:

1. Estructura de datos relacional: SQL Server es una base de datos relacional robusta que te permite almacenar y gestionar datos estructurados en tablas. Para un ToDo List, donde probablemente necesites almacenar información como tareas, fechas, estados, etc., una base de datos relacional es adecuada.

2. Transacciones y consistencia: SQL Server es conocido por su capacidad para manejar transacciones de manera segura y consistente. Esto es importante para asegurarte de que las operaciones en la base de datos, como agregar o eliminar tareas, sean confiables y no generen inconsistencias.

3. Integración con .NET Core: Dado que estás utilizando .NET Core en el backend, la integración con SQL Server será más fluida debido a las herramientas y bibliotecas disponibles, como Entity Framework, que simplifican la comunicación y el manejo de la base de datos.

## Consecuencias

1. Licenciamiento y costo: SQL Server puede tener costos asociados en términos de licencias y mantenimiento. Puedes evaluar si la versión Express o Developer es adecuada para tu proyecto en función de tus necesidades y restricciones presupuestarias.

2. Escalabilidad: Si bien SQL Server es escalable, ten en cuenta que las versiones gratuitas o de nivel de entrada pueden tener limitaciones en términos de rendimiento y escalabilidad en comparación con las versiones de gama alta.

3. Administración: La administración de SQL Server puede requerir cierto nivel de conocimientos y experiencia. Asegúrate de estar familiarizado con el proceso de configuración, respaldo y mantenimiento de la base de datos.

4. Compatibilidad multiplataforma: SQL Server originalmente estaba más orientado a entornos Windows, pero ha habido esfuerzos para hacerlo más compatible con plataformas como Linux, lo que te brinda más opciones en términos de infraestructura.

## Paradigma de SQLServer



## Características principales de SQLServer

- Gestión de Bases de Datos Relacionales: SQL Server es un sistema de gestión de bases de datos relacionales (RDBMS) que sigue el modelo relacional para organizar y administrar datos en tablas con filas y columnas.

- Lenguaje SQL: SQL Server utiliza el lenguaje SQL (Structured Query Language) para realizar operaciones en la base de datos, como consultas, inserciones, actualizaciones y eliminaciones.

- Transacciones y Control de Concurrencia: Ofrece soporte para transacciones ACID (Atomicidad, Consistencia, Aislamiento y Durabilidad), garantizando la integridad de los datos y el control de concurrencia para gestionar múltiples operaciones concurrentes de manera segura.

- Procedimientos Almacenados: Permite crear y ejecutar procedimientos almacenados, que son bloques de código SQL encapsulados que pueden aceptar parámetros y ser reutilizados, lo que mejora la modularidad y la seguridad.

- Vistas: SQL Server permite crear vistas, que son consultas predefinidas que se pueden utilizar como tablas virtuales. Las vistas ayudan a simplificar las consultas y a ocultar la complejidad subyacente.

- Seguridad y Autenticación: Proporciona mecanismos de autenticación y autorización para garantizar la seguridad de los datos. Los administradores pueden asignar permisos y roles a los usuarios para controlar su acceso a la base de datos.

- Optimización de Consultas: Incluye un optimizador de consultas que analiza y determina el plan de ejecución más eficiente para las consultas. Esto mejora el rendimiento y reduce el tiempo de respuesta.

- Alta Disponibilidad: Ofrece opciones para garantizar la alta disponibilidad de los datos, como la replicación, el clustering y las Always On Availability Groups, que permiten la conmutación por error y la recuperación rápida en caso de fallos.

- Soporte para Datos Espaciales: SQL Server ofrece capacidades avanzadas para manejar datos espaciales y geoespaciales, lo que es útil para aplicaciones de mapas y ubicación.

- Integración con Herramientas de Desarrollo: SQL Server se integra con varias herramientas de desarrollo, como Microsoft Visual Studio y SQL Server Management Studio (SSMS), que permiten administrar y desarrollar bases de datos de manera eficiente.

- Particionamiento de Tablas: Permite dividir tablas grandes en particiones más pequeñas, lo que mejora el rendimiento y facilita la administración de datos.

- Soporte para XML: SQL Server proporciona funcionalidad integrada para almacenar, consultar y transformar datos en formato XML.

- Business Intelligence (BI): Ofrece capacidades para el análisis de datos y la generación de informes, incluyendo servicios de análisis (Analysis Services) y servicios de informes (Reporting Services).

## Casos en los que se puede utilizar SQLServer

- Aplicaciones Empresariales: SQL Server se utiliza para desarrollar y gestionar bases de datos de aplicaciones empresariales, como sistemas de gestión de recursos humanos, sistemas de gestión de inventario, sistemas de contabilidad y sistemas de planificación de recursos empresariales (ERP).

- Sitios Web y Aplicaciones Web: SQL Server se integra con aplicaciones web y sitios web dinámicos para almacenar, recuperar y gestionar datos, como perfiles de usuarios, contenido dinámico y registros de actividad.

- Business Intelligence (BI): SQL Server se utiliza para almacenar y analizar datos para generar informes, tableros de control y análisis de datos para la toma de decisiones empresariales.

- Análisis de Datos: SQL Server ofrece capacidades de análisis que permiten realizar análisis en grandes conjuntos de datos para descubrir patrones, tendencias y relaciones.

- Aplicaciones Móviles: SQL Server se integra con aplicaciones móviles para almacenar y sincronizar datos de usuarios, como preferencias, datos de sesión y contenido personalizado.

- Gestión de Contenido: Se puede utilizar para administrar contenido y documentos, como sistemas de gestión de documentos y repositorios de contenido.

- Almacenamiento y Manipulación de Datos: SQL Server se usa para almacenar, recuperar y manipular datos en aplicaciones de todo tipo, desde pequeñas aplicaciones hasta aplicaciones empresariales críticas.

- Aplicaciones de Comercio Electrónico: Se utiliza en plataformas de comercio electrónico para almacenar información sobre productos, inventario, pedidos y transacciones.

- Sistemas de Soporte al Cliente: SQL Server se utiliza en aplicaciones de atención al cliente y gestión de relaciones con el cliente (CRM) para almacenar datos de clientes, historiales de interacciones y seguimiento de casos.

- Aplicaciones de Salud: En el sector de la salud, se utiliza para almacenar registros médicos electrónicos, información de pacientes y datos de investigación médica.

- Sistemas de Gestión de Aprendizaje: En el ámbito educativo, se utiliza para administrar datos de cursos, estudiantes y evaluaciones en sistemas de gestión de aprendizaje (LMS).

- Aplicaciones de Juegos en Línea: En el mundo de los juegos en línea, SQL Server puede utilizarse para administrar datos de jugadores, registros de partidas y clasificaciones.

- Sistemas de Control de Versiones: SQL Server se utiliza en sistemas de control de versiones para almacenar información sobre versiones de archivos y colaboración en equipo.

- Aplicaciones de Seguridad: Se utiliza en sistemas de seguridad para almacenar registros de eventos, información de acceso y seguimiento de actividad.

## Casos en los que no sería viable utilizar SQLServer

- Necesidades de Escalabilidad Extrema: Si estás diseñando una aplicación que requerirá un escalado masivo en términos de almacenamiento o rendimiento, podrías encontrar limitaciones en SQL Server. Aunque SQL Server puede escalar hasta cierto punto, existen soluciones más especializadas en bases de datos distribuidas y altamente escalables, como sistemas NoSQL y NewSQL.

- Altos Volúmenes de Escritura: Si tu aplicación genera una gran cantidad de operaciones de escritura intensivas en la base de datos y requiere una baja latencia en la escritura, es posible que SQL Server no sea la mejor opción. Algunas bases de datos NoSQL o sistemas de almacenamiento en memoria pueden ser más adecuados para este tipo de escenario.

- Modelos de Datos No Relacionales: Si tu aplicación trabaja con modelos de datos no relacionales, como datos en formato JSON o documentos semiestructurados, podría ser más conveniente utilizar una base de datos NoSQL, como MongoDB o Couchbase, que están diseñadas específicamente para manejar este tipo de datos.

- Análisis de Datos en Tiempo Real: Si necesitas realizar análisis de datos en tiempo real sobre flujos de datos continuos y cambiantes, es posible que una base de datos en memoria o soluciones de procesamiento de datos en tiempo real sean más apropiadas que SQL Server.

- Costos y Licenciamiento: En algunos casos, los costos asociados con las licencias de SQL Server pueden ser un factor limitante, especialmente en proyectos con presupuestos ajustados. En tales situaciones, considerar soluciones de código abierto o sistemas de bases de datos gratuitos puede ser más viable.

- Necesidades Geoespaciales Avanzadas: Aunque SQL Server tiene capacidades para datos espaciales, si tu aplicación requiere funcionalidades avanzadas de geolocalización y análisis geoespacial, podrías beneficiarte de soluciones especializadas en este campo.

- Requisitos de Portabilidad: Si deseas que tu aplicación sea altamente portátil y compatible con múltiples sistemas operativos y plataformas, SQL Server podría no ser la mejor opción, ya que está más estrechamente integrado con el ecosistema de Microsoft.

- Aplicaciones Altamente Distribuidas: Si estás construyendo una aplicación distribuida que abarca múltiples ubicaciones geográficas y requiere sincronización y replicación de datos complejos, podrías necesitar soluciones de bases de datos distribuidas más avanzadas.

- Experimentación y Desarrollo Rápido: En entornos en los que la flexibilidad y la agilidad son cruciales, herramientas NoSQL como bases de datos en memoria o almacenamiento de clave-valor pueden ser más adecuadas para el desarrollo rápido y la experimentación.