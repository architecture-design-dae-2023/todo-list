# BackEnd

## Estado

Propuesto

## Contexto

Para el proyecto se debe elegir la mejor herramienta entre Net Core y Quarkus.

## Decisión

Se toma la decisión de utilizar para el BackEnd la tecnología .Net Core. Por las siguientes razones:

1. Rendimiento y escalabilidad: .NET Core es conocido por su rendimiento eficiente y su capacidad para manejar cargas de trabajo escalables. Esto es especialmente importante si esperas que tu aplicación pueda manejar múltiples usuarios y tareas concurrentes.

2. Seguridad: .NET Core ofrece un alto nivel de seguridad. Microsoft constantemente realiza actualizaciones y parches de seguridad, lo que es fundamental para proteger los datos de los usuarios en una aplicación que maneja tareas y listas personales.

3. Facilidad de desarrollo: .NET Core tiene una sintaxis moderna y ofrece una amplia gama de herramientas y bibliotecas para agilizar el desarrollo. El uso de C# como lenguaje de programación también puede aumentar la productividad y mantener un código limpio y estructurado.

4. Integración con SQL Server: Dado que planeas utilizar SQL Server como base de datos, la integración con .NET Core será fluida. Entity Framework, por ejemplo, es una herramienta popular para mapear objetos a bases de datos relacionales, lo que facilita la gestión de datos.

## Consecuencias

1. Costo: Microsoft ofrece .NET Core como una tecnología de código abierto, lo que reduce los costos de licencia y facilita su implementación.

2. Curva de aprendizaje: Si no estás familiarizado con .NET Core, puede haber una curva de aprendizaje, pero hay una gran cantidad de recursos en línea para ayudarte a dominar la tecnología.

## Tecnologías utilizadas en Net 7:
- NET 7.
- API REST FULL.- Estilo de arquitectura de diseño de software que se utiliza para crear servicios web. 
- ASP .NET CORE WEB API.- Permitir la integración y la interacción entre aplicaciones o servicios que pueden estar en diferentes plataformas, servidores o dispositivos.
- Compatibilidad con Open Api.- Significa que está disponible públicamente para que los desarrolladores puedan acceder a ella y utilizarla para crear sus propias aplicaciones, extensiones o integraciones.
- Swagger.- Documentar servicios web de manera más eficiente.
- DTO.- Es una estructura de datos que se utiliza para agrupar y transportar datos de manera eficiente y coherente.
- AutoMapper.- Su función principal es facilitar y agilizar la conversión de datos entre clases con propiedades similares pero con estructuras diferentes.
- Entity Framework.- Para manejo de conexiones con Base de Datos. 
- Autorización con Bearer Token JWT (apiKey).- Es un método comúnmente utilizado en el contexto de la autenticación y autorización en aplicaciones web y APIs. JWT.
- Encriptación de contraseñas con MD5, como alternativa se implementó .Net Identity pero alno haber distinción de Roles se quedó unicamente con Autenticación personalizada por contraseña Encriptada y con Autorización JWT .
- Patrón de repositorio.-  Su propósito principal es separar la lógica de negocio del código que interactúa con la capa de persistencia (base de datos, API externas, servicios web, etc.), lo que mejora la mantenibilidad y la flexibilidad del código.
- Manejo de Perfiles globales de Caché para métodos de consulta .
- Uso de Cors para hacer una API multidominio, ya que se está trabajando colaborativamente desde algunos equipos.




## Paradigma de .Net

- .NET no es simplemente un lenguaje de programación o una única tecnología, sino una plataforma que engloba una variedad de lenguajes, bibliotecas, frameworks y herramientas diseñadas para facilitar el desarrollo de una amplia gama de aplicaciones. En este enfoque integral, .NET ofrece un conjunto completo de herramientas para cubrir desde el desarrollo de aplicaciones de escritorio y web hasta la construcción de soluciones en la nube y aplicaciones móviles, pasando por el manejo de bases de datos y la implementación de servicios.

- Esta plataforma integral se esfuerza por brindar coherencia y consistencia en el desarrollo, lo que permite a los desarrolladores trabajar con una amplia variedad de componentes bajo un mismo paraguas. Además, .NET se ha adaptado con el tiempo para ser multiplataforma, lo que refuerza su enfoque en proporcionar soluciones modernas y flexibles que se adapten a las necesidades cambiantes de desarrollo de aplicaciones.

## Características principales de .Net

- Multiplataforma: .NET se ha vuelto multiplataforma. Puedes desarrollar y ejecutar aplicaciones .NET en sistemas operativos como Windows, macOS y diversas distribuciones de Linux.

- Lenguajes de programación: .NET admite varios lenguajes de programación, incluyendo C#, F#, Visual Basic y otros. Esto te permite elegir el lenguaje con el que te sientas más cómodo mientras aprovechas las capacidades de la plataforma.

- Bibliotecas y Frameworks: .NET proporciona una amplia variedad de bibliotecas y frameworks que simplifican el desarrollo de aplicaciones. Estos incluyen Windows Forms, Windows Presentation Foundation (WPF), ASP.NET, ASP.NET Core y Entity Framework para trabajar con bases de datos, entre otros.

- Programación Orientada a Objetos (POO): .NET se basa en un enfoque orientado a objetos, lo que facilita la creación de aplicaciones estructuradas y modulares utilizando clases, objetos y herencia.

- Programación Funcional: A lo largo del tiempo, .NET ha incorporado conceptos de programación funcional, como funciones de primera clase, expresiones lambda y métodos de extensión. Lenguajes como F# están diseñados para un enfoque funcional.

- Rendimiento y Eficiencia: .NET está diseñado para ofrecer un alto rendimiento y eficiencia en la ejecución de aplicaciones. Las optimizaciones de compilación Just-In-Time (JIT) y las características de gestión de memoria contribuyen a este rendimiento.

- Seguridad: .NET ofrece características de seguridad integradas, como verificación de tipos, manejo de excepciones y seguridad de acceso a nivel de código. También proporciona herramientas para asegurar aplicaciones y protegerlas contra amenazas comunes.

- Desarrollo Web: Tanto con ASP.NET como con ASP.NET Core, puedes desarrollar aplicaciones web robustas y escalables. ASP.NET Core es especialmente conocido por su rendimiento y la capacidad de desarrollar aplicaciones web multiplataforma.

- Desarrollo de Aplicaciones de Escritorio: .NET ofrece opciones para desarrollar aplicaciones de escritorio, como Windows Forms y WPF. Estos frameworks permiten la creación de interfaces de usuario ricas y atractivas.

- Interoperabilidad: .NET admite la interoperabilidad con otros sistemas y lenguajes, lo que te permite integrar componentes y bibliotecas de otros entornos.

- Desarrollo Móvil: A través de Xamarin, puedes utilizar .NET para desarrollar aplicaciones móviles para iOS y Android, lo que permite compartir código entre las plataformas.

- Herramientas de Desarrollo: Microsoft proporciona una amplia gama de herramientas de desarrollo para .NET, como Visual Studio y Visual Studio Code, que ofrecen un entorno de desarrollo integrado (IDE) poderoso y productivo.

## Casos en los que se puede utilizar Net

- Aplicaciones de Escritorio:
    - Desarrollo de aplicaciones de negocio para empresas.
    - Creación de herramientas y utilidades de escritorio.
    - Aplicaciones científicas y de análisis de datos.
    - Software de diseño y modelado.

- Aplicaciones Web:
    - Desarrollo de sitios web dinámicos y aplicaciones web.
    - Plataformas de comercio electrónico y tiendas en línea.
    - Aplicaciones de administración y paneles de control.
    - Sistemas de gestión de contenidos (CMS).

- Aplicaciones Móviles:
    - Desarrollo de aplicaciones móviles nativas para iOS y Android utilizando Xamarin.
    - Aplicaciones de productividad, entretenimiento y comunicación.

- Aplicaciones Empresariales:
    - Desarrollo de sistemas de gestión empresarial (ERP, CRM).
    - Herramientas de automatización y optimización de procesos.
    - Soluciones de recursos humanos y gestión de proyectos.

- Aplicaciones en la Nube:
    - Desarrollo de servicios en la nube y aplicaciones basadas en microservicios.
    - Aplicaciones web escalables y de alto rendimiento.

- Juegos:
    - Desarrollo de juegos utilizando la plataforma Unity con C#.
    - Creación de juegos para consolas, PC y dispositivos móviles.

- Aplicaciones de Realidad Virtual y Aumentada:
    - Creación de experiencias de realidad virtual y aumentada.
    - Aplicaciones de entrenamiento, educación y entretenimiento.

- Herramientas de Desarrollo y Automatización:
    - Creación de herramientas de desarrollo y pruebas automatizadas.
    - Automatización de procesos y flujos de trabajo.

- Sistemas Integrados:
    - Desarrollo de soluciones integradas y dispositivos conectados.
    - Aplicaciones para el Internet de las cosas (IoT).

- Proyectos de Código Abierto:
    - Contribución a proyectos de código abierto basados en .NET.
    - Desarrollo de bibliotecas y componentes reutilizables.

## Casos en los que no sería viable utilizar Net

- Restricciones de Plataforma:
    - Si estás desarrollando una aplicación para una plataforma en la que .NET no tiene un buen soporte, como sistemas embebidos o dispositivos con recursos muy limitados, podría ser más adecuado utilizar una tecnología diferente.

- Requisitos de Rendimiento Extremo:
    - Si estás desarrollando aplicaciones que requieren un rendimiento extremadamente alto y cercano a la metal, como sistemas de trading de alta frecuencia o simulaciones de física complejas, podrías considerar lenguajes y plataformas más orientados al rendimiento, como C++.

- Condiciones de Latencia Extremadamente Baja:
    - Si estás construyendo aplicaciones en tiempo real con requisitos de latencia extremadamente baja, como sistemas de procesamiento de señales en tiempo real, podrías optar por tecnologías específicas que estén diseñadas para minimizar la latencia.

- Aplicaciones Exclusivamente para Dispositivos Móviles:
    - Si estás desarrollando aplicaciones móviles y deseas una experiencia altamente optimizada y nativa para cada plataforma (iOS y Android), es posible que prefieras utilizar lenguajes y herramientas específicas para cada plataforma, como Swift para iOS y Kotlin para Android.

- Restricciones de Hardware o Recursos:
    - Si estás desarrollando para plataformas con recursos muy limitados, como microcontroladores, podría ser más apropiado utilizar lenguajes y tecnologías diseñadas específicamente para este tipo de entornos.

- Necesidades Específicas de la Industria:
    - Si trabajas en una industria con requisitos específicos y tecnologías estándar establecidas, como aplicaciones financieras con tecnologías muy específicas, podría ser más adecuado utilizar herramientas y lenguajes preferidos en esa industria.

- Condiciones de Tiempo y Presupuesto Limitados:
    - Si tienes un plazo de tiempo muy ajustado o un presupuesto limitado, y tu equipo de desarrollo no está familiarizado con .NET, puede que sea más conveniente utilizar tecnologías con las que el equipo ya esté familiarizado para acelerar el proceso de desarrollo.

- Necesidades de Portabilidad Extrema:
    - Si necesitas crear aplicaciones altamente portables que se ejecuten en una amplia variedad de plataformas y sistemas operativos de manera uniforme, es posible que otras tecnologías multiplataforma más consolidadas sean más apropiadas.