# FrontEnd

## Estado
Propuesto

## Contexto
Para el proyecto se debe trabajar con la herramienta Vue.js.

## Decisión

Se toma la decisión de utilizar para el FrontEnd la tecnología Vue.js, poor las siguientes razones:

1. Interfaz de usuario interactiva: Vue.js es conocido por su enfoque reactivo y su capacidad para crear interfaces de usuario interactivas y dinámicas. Esto es perfecto para un ToDo List, ya que los usuarios podrán agregar, editar y eliminar tareas sin recargar la página.

2. Componentización: Vue.js fomenta la creación de componentes reutilizables, lo que puede acelerar el desarrollo y mantenimiento de la interfaz de usuario.

3. Rendimiento: Vue.js tiene un rendimiento rápido debido a su arquitectura basada en componentes y su enfoque en la virtualización del DOM.

4. Flexibilidad: Puedes elegir cómo estructurar tu aplicación, ya sea utilizando Vue CLI, Vuex para la gestión del estado, o simplemente integrando Vue.js en tu proyecto de manera incremental.

## Consecuencias

1. Tamaño del ecosistema: Aunque Vue.js es muy eficiente, el ecosistema podría ser más pequeño en comparación con frameworks como React o Angular. Sin embargo, para un proyecto de ToDo List, esto no debería ser un problema significativo.

2. Actualizaciones y compatibilidad: Al igual que con cualquier tecnología, asegúrate de estar al tanto de las actualizaciones de Vue.js para evitar problemas de compatibilidad en el futuro.

## Tecnologías utilizadas en Vue.js:
- axios (^1.4.0): Axios es una librería de JavaScript que se utiliza para hacer solicitudes HTTP desde el navegador o desde Node.js. Proporciona una interfaz fácil de usar para enviar y recibir datos a través de solicitudes HTTP, como GET, POST, PUT, DELETE, etc. Es especialmente útil para realizar llamadas a APIs y obtener o enviar datos en aplicaciones web.

- bootstrap (^5.3.1): Bootstrap es un popular framework de diseño y front-end que proporciona una colección de componentes, estilos y utilidades predefinidos para construir interfaces de usuario responsivas y atractivas. Ayuda a estandarizar y agilizar el proceso de diseño y desarrollo de sitios web y aplicaciones.

- bootstrap-vue (^2.23.1): Bootstrap-Vue es una integración de Bootstrap para Vue.js. Proporciona componentes de interfaz de usuario Vue.js que están diseñados siguiendo las pautas y el estilo de Bootstrap. Esto permite que los desarrolladores utilicen la funcionalidad de Bootstrap directamente en sus componentes Vue.

- core-js (^3.8.3): Core-js es una biblioteca que proporciona polyfills (rellenos) para características de JavaScript que pueden no ser compatibles con todos los navegadores. Esto asegura que tu código funcione de manera consistente en diferentes navegadores y versiones de ECMAScript.

- jwt-decode (^3.1.2): jwt-decode es una pequeña biblioteca que se utiliza para decodificar tokens JSON Web (JWT) en JavaScript. Las JWT son utilizadas comúnmente para autenticación y autorización en aplicaciones web y móviles. Esta biblioteca te permite decodificar el contenido de un token JWT sin necesidad de enviar la solicitud al servidor.

- sweetalert2 (^11.7.22): SweetAlert2 es una biblioteca que te permite crear ventanas modales personalizadas y atractivas en tus aplicaciones web. Estas ventanas modales se utilizan para mostrar mensajes, confirmaciones, alertas y otros tipos de interacciones con los usuarios.

- vue (^2.7.14): Vue.js es un framework progresivo de JavaScript utilizado para construir interfaces de usuario interactivas. Permite crear componentes reutilizables y dinámicos, y se centra en la creación de aplicaciones web de una sola página (SPA) con una estructura de componentes clara.

- vue-router (^3.6.5): Vue Router es la librería oficial de enrutamiento para Vue.js. Permite gestionar la navegación en una aplicación Vue de una manera declarativa, permitiendo que los componentes se muestren y oculten según la URL actual.

## Paradigma de Vue.js

- Programación Declarativa: Vue.js se basa en la programación declarativa, lo que significa que los desarrolladores describen cómo debería ser la interfaz de usuario en función del estado actual de la aplicación, en lugar de escribir lógica imperativa para manipular directamente el DOM (Document Object Model). Esto hace que el código sea más legible y fácil de mantener.

- Reactividad: Vue.js utiliza un sistema de reactividad para mantener automáticamente la interfaz de usuario sincronizada con los cambios en los datos subyacentes. Cuando los datos cambian, las partes relevantes de la interfaz de usuario se actualizan automáticamente sin necesidad de manipular directamente el DOM.

- Componentización: Vue.js fomenta la construcción de aplicaciones a través de componentes reutilizables. Los componentes son bloques autónomos de código que encapsulan tanto el comportamiento como la interfaz de usuario. Esto promueve la modularidad y facilita la reutilización de código.

- Directivas: Vue.js proporciona directivas especiales que se aplican directamente al DOM para agregar interactividad y manipulación de datos. Ejemplos de directivas son v-if, v-for, v-bind y v-on.

- Enlace de Datos Bidireccional: Vue.js facilita el enlace de datos bidireccional entre los componentes y el estado subyacente. Esto significa que los cambios en los datos se reflejan automáticamente en la interfaz de usuario y, a su vez, los cambios en la interfaz de usuario pueden actualizar los datos subyacentes.

## Características principales de Vue.js

- Virtual DOM: Vue.js utiliza un Virtual DOM para optimizar el rendimiento al minimizar las actualizaciones reales del DOM. Esto resulta en una experiencia más rápida y eficiente para los usuarios.

- Directivas y Filtros: Vue.js proporciona directivas como v-if, v-for, v-bind y v-on para manipular el DOM de manera declarativa. También admite filtros para formatear datos de manera fácil y legible en la interfaz de usuario.

- Gestión de Estado: Aunque Vue.js no impone un enfoque específico para la gestión del estado, se integra bien con bibliotecas como Vuex y Redux para manejar estados complejos de manera eficiente.

- Routing: Vue Router es un complemento oficial que facilita la implementación de enrutamiento en aplicaciones Vue.js, permitiendo la navegación entre diferentes vistas.

- Transiciones y Animaciones: Vue.js proporciona formas de aplicar transiciones y animaciones a elementos cuando se agregan, actualizan o eliminan de la interfaz de usuario.

- Extensibilidad: Vue.js es altamente extensible y permite integrar bibliotecas y herramientas externas de manera fácil.

## Casos en los que se puede utilizar Vue.js

- Interfaces de Usuario Interactivas: Vue.js es especialmente adecuado para construir interfaces de usuario altamente interactivas, como paneles de control, tableros de administración y aplicaciones de análisis de datos. Su sistema de reactividad facilita la actualización en tiempo real de los datos y la interfaz de usuario.

- Páginas y Sitios Web Sencillos: Si estás desarrollando una página web o un sitio web relativamente simple, Vue.js puede ser una elección ligera y eficiente. Puede ayudarte a agregar interactividad y dinamismo sin la complejidad de un framework más grande.

- Componentización y Reutilización: Vue.js es especialmente efectivo cuando deseas construir componentes reutilizables. Puedes crear componentes individuales para elementos como barras de navegación, tarjetas, formularios y más, lo que facilita el mantenimiento y la expansión de tus aplicaciones.

- Aplicaciones de Una Página (SPA): Vue.js es una excelente opción para construir aplicaciones de una sola página (SPA), donde toda la interacción ocurre en una única página web. Vue Router, el enrutador oficial de Vue.js, permite crear una navegación fluida entre las diferentes secciones de la SPA.

- Aplicaciones Web en Tiempo Real: Si necesitas desarrollar una aplicación web en tiempo real, como chats en vivo, seguimiento de ubicación en tiempo real o colaboración en línea, Vue.js puede ser una elección adecuada. Su sistema de reactividad facilita la actualización constante de la interfaz de usuario con los datos en tiempo real.

- Prototipado Rápido: Gracias a su sintaxis intuitiva y facilidad de uso, Vue.js es una excelente opción para prototipar rápidamente ideas y conceptos. Puedes crear prototipos interactivos en poco tiempo y luego decidir si deseas escalarlos a aplicaciones más grandes.

- Aplicaciones Híbridas y Móviles: A través de frameworks como Vue Native o Quasar, Vue.js se puede utilizar para desarrollar aplicaciones móviles y híbridas que funcionan en plataformas como iOS y Android. Esto permite compartir gran parte del código entre la versión web y la versión móvil de la aplicación.

- Integración con Otras Tecnologías: Vue.js se integra bien con otras tecnologías y bibliotecas, lo que te permite combinarlo con herramientas como Vuex para la gestión de estado, Vue Router para la navegación y Axios para realizar solicitudes HTTP.

## Casos en los que no sería viable utilizar Vue.js
 
- Aplicaciones Altamente Complejas: Si estás construyendo una aplicación web extremadamente compleja con una gran cantidad de interacciones y estado compartido, otros frameworks como React (con el uso de Redux) o Angular podrían ser más adecuados. Estos frameworks ofrecen estructuras más robustas para gestionar estados complejos y arquitecturas escalables.

- Equipo con Experiencia Especializada: Si tu equipo de desarrollo ya tiene experiencia y especialización en otros frameworks como React o Angular, podría ser más conveniente seguir con lo que conocen en lugar de introducir Vue.js y requerir tiempo adicional de aprendizaje.

- Soporte Corporativo y Largo Plazo: Si estás trabajando en una organización grande que prioriza el soporte corporativo a largo plazo y la estabilidad, podría ser más adecuado optar por frameworks respaldados por grandes empresas, como Angular (Google) o React (Facebook), que ofrecen recursos más sólidos en términos de soporte y mantenimiento.

- Aplicaciones de Tiempo Real Extremo: Si tu aplicación se basa en una gran cantidad de tiempo real y comunicación en tiempo real, especialmente en situaciones de alta demanda, podrías optar por frameworks más especializados en este ámbito, como Meteor.

- Dificultades con Integración: Si tienes una infraestructura tecnológica existente que no se integra fácilmente con Vue.js, o si tu equipo enfrenta dificultades al intentar integrar Vue.js con herramientas o sistemas existentes, podría ser más conveniente optar por una tecnología que encaje mejor con tu entorno actual.

- Desarrollo de Aplicaciones Nativas: Si tu objetivo es desarrollar aplicaciones móviles nativas sin compromisos de rendimiento y con acceso completo a las capacidades del dispositivo, es posible que quieras considerar lenguajes y herramientas específicos para plataformas móviles, como Swift para iOS y Kotlin para Android.